import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/data/network/endpoints/office.endpoint.dart';
import 'package:starter/data/network/responses/quote.response.dart';
import 'package:starter/presentation/ui/viewmodels/home/home.viewstate.dart';

class HomeViewModel extends BaseViewModel<HomeViewState>  {
  ///
  OfficeEndpoint officeEndpoint;

  ///
  StreamSubscription? streamSubscription;

  ///
  HomeViewModel({required this.officeEndpoint})
      : super(HomeViewState.initial());

  ///
  Future<void> init() async {
    try {
      setState(state.copyWith(isLoading: true));
      QuoteResponse response = await officeEndpoint.getQuote();
      setState(state.copyWith(quote: response));
    } catch (e) {
      debugPrint(e.toString());
    } finally {
      setState(state.copyWith(isLoading: false));
    }
  }

  ///
  Future<void> refresh(BuildContext context) async {
    showLoader(context);
    try {
      QuoteResponse response = await officeEndpoint.getQuote();
      setState(state.copyWith(quote: response));
    } catch (e) {
      debugPrint(e.toString());
    }
    if (context.mounted) hideLoader(context);
  }

  @override
  void dispose() {
    streamSubscription?.cancel();
    super.dispose();
  }
}
