import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:starter/data/injection/injector.dart';
import 'package:starter/data/network/endpoints/office.endpoint.dart';
import 'package:starter/presentation/ui/viewmodels/home/home.viewmodel.dart';

///
/// [HomeViewModelProvider]
///
class HomeViewModelProvider extends StatelessWidget {
  /// Widget builder
  final Widget Function(BuildContext context, Widget? child)? builder;

  ///
  final Widget? child;

  ///
  const HomeViewModelProvider({required this.builder, super.key, this.child});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: builder,
      create: (BuildContext context) {
        return HomeViewModel(officeEndpoint: injector<OfficeEndpoint>())
          ..init();
      },
    );
  }
}
