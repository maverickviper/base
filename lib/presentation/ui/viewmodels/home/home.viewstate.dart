import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/data/network/responses/quote.response.dart';

part 'home.viewstate.g.dart';

@CopyWith()
class HomeViewState extends ViewState {
  final bool isLoading;

  final QuoteResponse? quote;

  ///
  HomeViewState({required this.isLoading,required this.quote});

  ///
  /// Named Constructor for initial state
  ///
  HomeViewState.initial() : isLoading = false,quote = null;

  ///
  /// Props
  ///
  @override
  List<Object?> get props => <Object?>[isLoading,quote];
}
