import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:starter/presentation/ui/viewmodels/profile/profile_viewmodel.dart';

class ProfileViewModelProvider extends StatelessWidget {
  /// Widget builder
  final Widget Function(BuildContext context, Widget? child)? builder;

  ///
  final Widget? child;

  const ProfileViewModelProvider({required this.builder, super.key, this.child});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => ProfileViewModel(), builder: builder, child: child,);
  }
}
