import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';

part 'profile_state.g.dart';

@CopyWith()
class ProfileViewState extends ViewState {
  final bool? isLoading;

  ProfileViewState({required this.isLoading});

  ProfileViewState.initial() : isLoading = false;

  @override
  List<Object?> get props => <Object?>[isLoading];
}
