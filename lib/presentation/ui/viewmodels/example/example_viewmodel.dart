import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/data/models/example.model.dart';
import 'package:starter/presentation/ui/viewmodels/example/example_viewstate.dart';

///
/// [ExampleViewModel]
///
class ExampleViewModel extends BaseViewModel<ExampleViewState> {
  ///
  ExampleViewModel() : super(ExampleViewState.initial());

  ///
  /// Just Initializing with 20/20 green and red boxes
  ///
  ///
  /// One MUST have good understanding about copying & equality checking
  /// between objects to understand the [setState] method.
  ///
  void init() {
    setState(state.copyWith(
      redBoxes: List<RedBox>.generate(20, (int index) => const RedBox()),
      greenBoxes: List<GreenBox>.generate(20, (int index) => const GreenBox()),
    ),);
  }

  ///
  void updateGreenBox(int pos) {
    List<GreenBox> greenItems = <GreenBox>[...state.greenBoxes!];
    greenItems[pos] =
        greenItems[pos].copyWith(isSelected: !greenItems[pos].isSelected);
    setState(state.copyWith(greenBoxes: greenItems));
  }

  ///
  void updateRedBox(int pos) {
    List<RedBox> redItems = <RedBox>[...state.redBoxes!];
    redItems[pos] =
        redItems[pos].copyWith(isSelected: !redItems[pos].isSelected);
    setState(state.copyWith(redBoxes: redItems));
  }
}
