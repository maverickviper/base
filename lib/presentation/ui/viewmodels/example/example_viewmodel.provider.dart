import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:starter/presentation/ui/viewmodels/example/example_viewmodel.dart';

///
/// [ExampleViewModelProvider]
///
class ExampleViewModelProvider extends StatelessWidget {

  /// Widget builder
  final Widget Function(BuildContext context, Widget? child)? builder;

  ///
  final Widget? child;

  ///
  const ExampleViewModelProvider({required this.builder, super.key, this.child});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: builder,
      create: (BuildContext context) {
        return ExampleViewModel()..init();
      },
    );
  }
}
