// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'example_viewstate.dart';

// **************************************************************************
// CopyWithGenerator
// **************************************************************************

abstract class _$ExampleViewStateCWProxy {
  ExampleViewState isLoading(bool isLoading);

  ExampleViewState redBoxes(List<RedBox>? redBoxes);

  ExampleViewState greenBoxes(List<GreenBox>? greenBoxes);

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ExampleViewState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ExampleViewState(...).copyWith(id: 12, name: "My name")
  /// ````
  ExampleViewState call({
    bool? isLoading,
    List<RedBox>? redBoxes,
    List<GreenBox>? greenBoxes,
  });
}

/// Proxy class for `copyWith` functionality. This is a callable class and can be used as follows: `instanceOfExampleViewState.copyWith(...)`. Additionally contains functions for specific fields e.g. `instanceOfExampleViewState.copyWith.fieldName(...)`
class _$ExampleViewStateCWProxyImpl implements _$ExampleViewStateCWProxy {
  const _$ExampleViewStateCWProxyImpl(this._value);

  final ExampleViewState _value;

  @override
  ExampleViewState isLoading(bool isLoading) => this(isLoading: isLoading);

  @override
  ExampleViewState redBoxes(List<RedBox>? redBoxes) => this(redBoxes: redBoxes);

  @override
  ExampleViewState greenBoxes(List<GreenBox>? greenBoxes) =>
      this(greenBoxes: greenBoxes);

  @override

  /// This function **does support** nullification of nullable fields. All `null` values passed to `non-nullable` fields will be ignored. You can also use `ExampleViewState(...).copyWith.fieldName(...)` to override fields one at a time with nullification support.
  ///
  /// Usage
  /// ```dart
  /// ExampleViewState(...).copyWith(id: 12, name: "My name")
  /// ````
  ExampleViewState call({
    Object? isLoading = const $CopyWithPlaceholder(),
    Object? redBoxes = const $CopyWithPlaceholder(),
    Object? greenBoxes = const $CopyWithPlaceholder(),
  }) {
    return ExampleViewState(
      isLoading: isLoading == const $CopyWithPlaceholder() || isLoading == null
          ? _value.isLoading
          // ignore: cast_nullable_to_non_nullable
          : isLoading as bool,
      redBoxes: redBoxes == const $CopyWithPlaceholder()
          ? _value.redBoxes
          // ignore: cast_nullable_to_non_nullable
          : redBoxes as List<RedBox>?,
      greenBoxes: greenBoxes == const $CopyWithPlaceholder()
          ? _value.greenBoxes
          // ignore: cast_nullable_to_non_nullable
          : greenBoxes as List<GreenBox>?,
    );
  }
}

extension $ExampleViewStateCopyWith on ExampleViewState {
  /// Returns a callable class that can be used as follows: `instanceOfExampleViewState.copyWith(...)` or like so:`instanceOfExampleViewState.copyWith.fieldName(...)`.
  // ignore: library_private_types_in_public_api
  _$ExampleViewStateCWProxy get copyWith => _$ExampleViewStateCWProxyImpl(this);
}
