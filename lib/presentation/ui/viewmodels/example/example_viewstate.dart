import 'package:copy_with_extension/copy_with_extension.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/data/models/example.model.dart';

part 'example_viewstate.g.dart';

@CopyWith()
class ExampleViewState extends ViewState {
  final bool isLoading;

  final List<RedBox>? redBoxes;

  final List<GreenBox>? greenBoxes;

  ///
  ExampleViewState(
      {required this.isLoading,
      required this.redBoxes,
      required this.greenBoxes,});

  ///
  /// Named Constructor for initial state
  ///
  ExampleViewState.initial()
      : isLoading = false,
        redBoxes = null,
        greenBoxes = null;

  ///
  /// Props
  ///
  @override
  List<Object?> get props => <Object?>[isLoading, redBoxes, greenBoxes];
}
