import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:starter/presentation/styles/text_variants.dart';
import 'package:starter/presentation/ui/viewmodels/profile/profile_viewmodel_provider.dart';

@RoutePage()
class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ProfileViewModelProvider(builder: (_, __) {
      return Scaffold(
        appBar: AppBar(
          title: const TextVariant(
              data: 'Profile', variantType: TextVariantType.displayMedium,),
        ),
        body: const _ProfileBody(),
      );
    },);
  }
}

class _ProfileBody extends StatelessWidget {
  const _ProfileBody();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: TextButton(
        onPressed: () {
        },
        child: const TextVariant(
          data: 'Send Event',
          variantType: TextVariantType.labelLarge,
        ),
      ),
    );
  }
}
