import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:starter/application/routes/router.gr.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/presentation/styles/global.styles.dart';
import 'package:starter/presentation/styles/text_variants.dart';
import 'package:starter/presentation/ui/viewmodels/home/home.viewmodel.dart';
import 'package:starter/presentation/ui/viewmodels/home/home_viewmodel.provider.dart';

@RoutePage()
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return HomeViewModelProvider(builder: (_, __) {
      return Scaffold(
        appBar: AppBar(
          title: const TextVariant(
            data: 'Home',
            variantType: TextVariantType.headlineMedium,
          ),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  AutoRouter.of(context).push(const ExampleRoute());
                },
                icon: const Icon(Icons.navigate_next),),
          ],
        ),
        body: const _HomeBody(),
      );
    },);
  }
}

///
/// private widget
///
class _HomeBody extends StatelessWidget {
  const _HomeBody();

  @override
  Widget build(BuildContext context) {
    HomeViewModel viewModel = BaseViewModel.watch<HomeViewModel>(context);
    ColorScheme colorScheme = GlobalTheme.getColorScheme(context);
    return viewModel.state.isLoading
        ? const Center(
            child: CircularProgressIndicator(),
          )
        : Padding(
            padding: const EdgeInsets.all(16),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  TextVariant(
                      color: colorScheme.onSurface,
                      textAlign: TextAlign.center,
                      data: viewModel.state.quote?.sentence ?? 'Empty',
                      variantType: TextVariantType.titleLarge,),
                  const SizedBox(height: 16),
                  Align(
                      alignment: Alignment.centerRight,
                      child: TextVariant(
                        data:
                            '- ${viewModel.state.quote?.character?.name ?? ''}',
                        variantType: TextVariantType.bodyLarge,
                        color: colorScheme.primary,
                      ),),
                  const SizedBox(height: 20),
                  ElevatedButton(
                      onPressed: () {
                        viewModel.refresh(context);
                      },
                      child: const TextVariant(
                        data: 'Refresh',
                      ),),
                ],
              ),
            ),
          );
  }
}
