import 'dart:developer';

import 'package:auto_route/annotations.dart';
import 'package:flutter/material.dart';
import 'package:starter/data/abstracts/abstract.viewmodel.dart';
import 'package:starter/data/models/example.model.dart';
import 'package:starter/presentation/styles/text_variants.dart';
import 'package:starter/presentation/ui/viewmodels/example/example_viewmodel.dart';
import 'package:starter/presentation/ui/viewmodels/example/example_viewmodel.provider.dart';

///
/// [ExampleScreen]
///
/// ************ This Example will show you when to use select,read
/// and watch methods of the Provider ************
///
/// The method "watch" will always refresh the widget
/// (i.e. build() will be invoked when notifyListeners() is called)
/// so use it wisely.
///
/// The method "read" is to access viewModel properties without refreshing the widget
/// So Widgets which uses "read" method won't refresh when notifyListeners() called.
///
/// The method "select" will refresh the widget only when the selected object of the viewmodel
/// changes
///
///
@RoutePage()
class ExampleScreen extends StatelessWidget {
  const ExampleScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return ExampleViewModelProvider(builder: (_, __) {
      return Scaffold(
        appBar: AppBar(
          title: const TextVariant(
              data: 'Example', variantType: TextVariantType.titleMedium,),
        ),
        body: const _ExampleBody(),
      );
    },);
  }
}

class _ExampleBody extends StatelessWidget {
  const _ExampleBody();

  @override
  Widget build(BuildContext context) {
    log('<<<<<< $this Refreshed >>>>>');
    return const Padding(
      padding: EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          _GreenBoxListWidget(),
          SizedBox(height: 20),
          _RedBoxListWidget(),
          SizedBox(height: 20),
          _TotalCalculationWidget(),
        ],
      ),
    );
  }
}

///
/// This widget only reacts to `viewModel.state.redBoxes` changes
/// see log
///
/// If this BaseViewModel.select syntax not understandable
/// please check [_LoadingWidget]'s Selector
class _RedBoxListWidget extends StatelessWidget {
  const _RedBoxListWidget();

  @override
  Widget build(BuildContext context) {
    log('<<<<<< $this Refreshed >>>>>');
    ExampleViewModel viewModel = BaseViewModel.read<ExampleViewModel>(context);
    List<RedBox> redBoxes =
        BaseViewModel.select<ExampleViewModel, List<RedBox>>(
            context, (ExampleViewModel viewModel) => viewModel.state.redBoxes!,);
    return SizedBox(
      height: 200,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, int pos) {
          RedBox item = redBoxes[pos];
          return InkWell(
            borderRadius: BorderRadius.circular(12),
            onTap: () {
              viewModel.updateRedBox(pos);
            },
            child: SizedBox(
              height: 180,
              width: 150,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: item.color,),
                  ),
                  if (item.isSelected)
                    const Align(
                        child: Icon(Icons.done, size: 36),),
                ],
              ),
            ),
          );
        },
        separatorBuilder: (_, __) {
          return const SizedBox(width: 16);
        },
        itemCount: redBoxes.length,
      ),
    );
  }
}

///
/// This widget only reacts to `viewModel.state.greenBoxes` changes
/// see log
///
/// If this BaseViewModel.select syntax not understandable
/// please check [_LoadingWidget]'s Selector
class _GreenBoxListWidget extends StatelessWidget {
  const _GreenBoxListWidget();

  @override
  Widget build(BuildContext context) {
    log('<<<<<< $this Refreshed >>>>>');
    ExampleViewModel viewModel = BaseViewModel.read<ExampleViewModel>(context);
    List<GreenBox> greenBoxes =
        BaseViewModel.select<ExampleViewModel, List<GreenBox>>(context,
            (ExampleViewModel viewModel) => viewModel.state.greenBoxes!,);
    return SizedBox(
      // we can use SingleChildScrollView + Row to avoid static height but it affect performance
      height: 200,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        scrollDirection: Axis.horizontal,
        itemBuilder: (_, int pos) {
          GreenBox item = greenBoxes[pos];
          return InkWell(
            borderRadius: BorderRadius.circular(12),
            onTap: () {
              viewModel.updateGreenBox(pos);
            },
            child: SizedBox(
              height: 180,
              width: 150,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(12),
                        color: item.color,),
                  ),
                  if (item.isSelected)
                    const Align(
                        child: Icon(Icons.done, size: 36),),
                ],
              ),
            ),
          );
        },
        separatorBuilder: (_, __) {
          return const SizedBox(width: 16);
        },
        itemCount: greenBoxes.length,
      ),
    );
  }
}

///
/// This widget reacts to all changes
/// Because we are using "watch" method
/// see log
///
class _TotalCalculationWidget extends StatelessWidget {
  const _TotalCalculationWidget();

  @override
  Widget build(BuildContext context) {
    log('<<<<<< $this Refreshed >>>>>');
    ExampleViewModel viewModel = BaseViewModel.watch<ExampleViewModel>(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: <Widget>[
          TextVariant(
              data:
                  'Total Green Boxes Selected  ==> ${viewModel.state.greenBoxes?.where((GreenBox element) => element.isSelected).length}',),
          TextVariant(
              data:
                  'Total Red Boxes Selected  ==> ${viewModel.state.redBoxes?.where((RedBox element) => element.isSelected).length}',),
        ],
      ),
    );
  }
}

