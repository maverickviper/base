// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i174;
import 'package:injectable/injectable.dart' as _i526;
import 'package:shared_preferences/shared_preferences.dart' as _i460;
import 'package:starter/data/abstracts/abstract.storage.dart' as _i302;
import 'package:starter/data/injection/modules.dart' as _i964;
import 'package:starter/data/network/dio.client.dart' as _i576;
import 'package:starter/data/network/endpoints/office.endpoint.dart' as _i719;
import 'package:starter/data/network/interceptors/activity.interceptor.dart'
    as _i399;
import 'package:starter/data/services/dialog.service.dart' as _i860;
import 'package:starter/data/storage/local/local.storage.dart' as _i921;
import 'package:starter/presentation/styles/global.styles.dart' as _i118;

extension GetItInjectableX on _i174.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  Future<_i174.GetIt> init({
    String? environment,
    _i526.EnvironmentFilter? environmentFilter,
  }) async {
    final gh = _i526.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final injectorModule = _$InjectorModule();
    await gh.singletonAsync<_i460.SharedPreferences>(
      () => injectorModule.prefs,
      preResolve: true,
    );
    gh.singleton<_i860.DialogService>(() => _i860.DialogService());
    gh.singleton<_i118.GlobalTheme>(() => _i118.GlobalTheme());
    gh.singleton<_i302.StorageInterface>(
        () => _i921.LocalStorage(gh<_i460.SharedPreferences>()));
    gh.factory<String>(
      () => injectorModule.baseUrl,
      instanceName: 'baseUrl',
    );
    gh.lazySingleton<_i576.DioClient>(
        () => injectorModule.dio(gh<String>(instanceName: 'baseUrl')));
    gh.factory<_i719.OfficeEndpoint>(
        () => _i719.OfficeEndpoint(gh<_i576.DioClient>()));
    gh.singleton<_i399.ActivityInterceptor>(
        () => _i399.ActivityInterceptor(gh<_i576.DioClient>()));
    return this;
  }
}

class _$InjectorModule extends _i964.InjectorModule {}
