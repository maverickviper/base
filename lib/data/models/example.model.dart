import 'package:flutter/material.dart';

///
/// Sample
///
class RedBox {
  final Color color;

  final bool isSelected;

  const RedBox({this.color = Colors.red, this.isSelected = false});

  RedBox copyWith({Color? color, bool? isSelected}) {
    return RedBox(
        color: color ?? this.color, isSelected: isSelected ?? this.isSelected,);
  }
}

class GreenBox {
  final Color color;

  final bool isSelected;

  const GreenBox({this.color = Colors.green, this.isSelected = false});

  GreenBox copyWith({Color? color, bool? isSelected}) {
    return GreenBox(
        color: color ?? this.color, isSelected: isSelected ?? this.isSelected,);
  }
}
