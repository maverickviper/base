import 'package:flutter/material.dart';
import 'package:starter/application/config/app_config.dart';
import 'package:starter/application/config/application.dart';
import 'package:starter/application/routes/router.dart';
import 'package:starter/presentation/styles/themes.dart';
import 'package:easy_localization/easy_localization.dart';

Future<void> startApplication(AppConfiguration configuration) async {
  await Application.initialize(config: configuration);
  runApp(EasyLocalization(
      supportedLocales: const <Locale>[Locale('en')],
      path: 'assets/translations',
      fallbackLocale: const Locale('en'),
      child: MyApp(),
  ),
  );
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  final AppRouter router = AppRouter();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Flutter Demo',
      routerConfig: router.config(),
      locale: context.locale,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      theme: lightThemeData,
      darkTheme: darkThemeData,
    );
  }
}
