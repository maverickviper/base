import 'package:auto_route/auto_route.dart';
import 'package:starter/application/routes/router.gr.dart';


@AutoRouterConfig()
class AppRouter extends RootStackRouter {
  @override
  RouteType get defaultRouteType => const RouteType.material();

  @override
  List<AutoRoute> get routes => <AutoRoute>[
        AdaptiveRoute(page: HomeRoute.page, initial: true),
        AdaptiveRoute(page: ProfileRoute.page, path: '/profile'),
        AdaptiveRoute(page: ExampleRoute.page, path: '/example'),
      ];
}
